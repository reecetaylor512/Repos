// Copyright Epic Games, Inc. All Rights Reserved.

#include "AGPAssignmentGameMode.h"
#include "AGPAssignmentHUD.h"
#include "AGPAssignmentCharacter.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/GameState.h"
#include "GameFramework/PlayerState.h"
#include "UObject/ConstructorHelpers.h"

AAGPAssignmentGameMode::AAGPAssignmentGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	if (PlayerPawnClassFinder.Class != NULL)
	{

		DefaultPawnClass = PlayerPawnClassFinder.Class;
	}
	// use our custom HUD class
	//HUDClass = AAGPAssignmentHUD::StaticClass();

	maxNumOfPlayers = 2; 
}

void AAGPAssignmentGameMode::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("BeginPlay"));
}
void AAGPAssignmentGameMode::PostLogin(APlayerController* newPC)
{
	Super::PostLogin(newPC);
	AGameStateBase* gs = GetGameState<AGameStateBase>();
	if (gs && gs->PlayerArray.Num() == maxNumOfPlayers) {
		UE_LOG(LogTemp, Warning, TEXT("PostLogin Num players: %d"), gs->PlayerArray.Num());
	}
	spawnGoalArea();
}

void AAGPAssignmentGameMode::stopGame()
{
	gameOver(false, -1);
}

void AAGPAssignmentGameMode::gameOver(bool hasWon, int winID)
{
	AGameStateBase* gs = GetGameState<AGameStateBase>();  //get server character
	AAGPAssignmentCharacter* chr = Cast<AAGPAssignmentCharacter>(gs->PlayerArray[0]->GetPawn());
	chr->MC_GameOver(hasWon, winID);
	GetWorldTimerManager().ClearTimer(gameTimer);
	GetWorldTimerManager().ClearTimer(secondsTimer);

}

void AAGPAssignmentGameMode::chkForWin(AGoalArea* goal, int ID)
{
	//if (goal->getnumberOfPlayersAtGoal() == maxNumOfPlayers)
		gameOver(true, ID);
}

void AAGPAssignmentGameMode::spawnGoalArea()
{
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	spawnParams.Instigator = GetInstigator();
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	FVector spawnLoc = FVector(FMath::RandRange(-spawnRange, spawnRange), 0.0f, 130.0f);
	FRotator rot = FRotator::ZeroRotator;
	GetWorld()->SpawnActor<AGoalArea>(goalAreaClass, spawnLoc, rot, spawnParams);
}