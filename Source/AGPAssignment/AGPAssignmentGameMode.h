// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GoalArea.h"
#include "AGPAssignmentGameMode.generated.h"

UCLASS(minimalapi)
class AAGPAssignmentGameMode : public AGameModeBase
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay();

public:
	AAGPAssignmentGameMode();

	virtual void PostLogin(APlayerController* newPC) override;

	FTimerHandle gameTimer;
	FTimerHandle secondsTimer;
	UPROPERTY(EditAnywhere)
		float gameDuration;
	UFUNCTION() //UFUNCTION needed for timer!!!
		void stopGame();
	void gameOver(bool hasWon, int winID);

	void chkForWin(AGoalArea* goal, int ID);
	UPROPERTY(EditAnywhere, Category = Goal)
		float spawnRange;
	UPROPERTY(EditAnywhere, Category = Goal)
		TSubclassOf<class AGoalArea> goalAreaClass;
	void spawnGoalArea();

	int maxNumOfPlayers;
};



