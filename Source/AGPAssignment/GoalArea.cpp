// Fill out your copyright notice in the Description page of Project Settings.


#include "GoalArea.h"
#include "Components/BoxComponent.h"
#include "AGPAssignmentCharacter.h"

// Sets default values
AGoalArea::AGoalArea()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PrimaryActorTick.bCanEverTick = false;
	collisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("collisionComponent"));
	RootComponent = collisionComponent; 
	staticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("staticMeshComponent"));
	staticMeshComponent->SetupAttachment(RootComponent); 
	staticMeshComponent->SetCollisionProfileName("OverlapAll");
	bReplicates = true; 
}

// Called when the game starts or when spawned
void AGoalArea::BeginPlay()
{
	Super::BeginPlay();
	collisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AGoalArea::OnBeginOverlap); 
}

// Called every frame
void AGoalArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGoalArea::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AAGPAssignmentCharacter* chr = Cast<AAGPAssignmentCharacter>(OtherActor);
	if (chr && chr->IsLocallyControlled()) {
		UE_LOG(LogTemp, Warning, TEXT("OnBeginOverlap chr = %s"), *chr->GetName());
		TArray<AActor*> Result; //Store ALL players at Goal

		GetOverlappingActors(Result, AAGPAssignmentCharacter::StaticClass());
		for (int i = 0; i < Result.Num(); i++)
		{
			chr = Cast<AAGPAssignmentCharacter>(Result[i]);
			addToPlayerIDsAtGoal(chr->GetPlayerID());
		}
		chr->updateGoalArea(this, chr->GetPlayerID()); 
	}
}

void AGoalArea::addToPlayerIDsAtGoal(int ID)
{
	for (int i = 0; i < playerIDsAtGoal.Num(); i++)
	{
		if (playerIDsAtGoal[i] == ID)
			return;
	}
	playerIDsAtGoal.Add(ID); 
}

int AGoalArea::getnumberOfPlayersAtGoal()
{
	return playerIDsAtGoal.Num();
}

