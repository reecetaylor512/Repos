// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "AGPAssignmentHUD.generated.h"

UCLASS()
class AAGPAssignmentHUD : public AHUD
{
	GENERATED_BODY()

public:
	AAGPAssignmentHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

